namespace GxPlayerStore.Models.Input
{
    /// <summary>
    /// Represents input model for store entity.
    /// </summary>
    public class StoreInputModel
    {
        /// <summary>
        /// Gets or sets store ID.
        /// </summary>
        /// <value>
        /// Store ID.
        /// </value>
        public string Id { get; set; }
    }
}
