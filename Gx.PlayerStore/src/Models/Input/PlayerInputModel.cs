namespace GxPlayerStore.Models.Input
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Represents input model for player.
    /// </summary>
    public class PlayerInputModel
    {
        /// <summary>
        /// Gets or sets player ID.
        /// </summary>
        /// <value>
        /// Player ID.
        /// </value>
        [Required]
        public string Id { get; set; }
    }
}
