namespace GxPlayerStore.Models.Input.Library
{
    /// <summary>
    /// Represents input model for library entity.
    /// </summary>
    public class GameLibraryInputModel
    {
        /// <summary>
        /// Gets or sets library ID.
        /// </summary>
        /// <value>
        /// Library ID.
        /// </value>
        public string Id { get; set; }
    }
}
