namespace GxPlayerStore.Models.Input.Market
{
    /// <summary>
    /// Represents market model for selling.
    /// </summary>
    public class MarketSellGameInputModel
    {
        /// <summary>
        /// Gets or sets library ID.
        /// </summary>
        /// <value>
        /// Library ID.
        /// </value>
        public string LibraryGameId { get; set; }

        /// <summary>
        /// Gets or sets game cost.
        /// </summary>
        /// <value>
        /// Game cost.
        /// </value>
        public decimal Cost { get; set; }
    }
}
