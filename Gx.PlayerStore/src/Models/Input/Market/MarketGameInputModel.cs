namespace GxPlayerStore.Models.Input.Market
{
    /// <summary>
    /// Represents input model for market.
    /// </summary>
    public class MarketGameInputModel
    {
        /// <summary>
        /// Gets or sets market ID.
        /// </summary>
        /// <value>
        /// Market ID.
        /// </value>
        public string Id { get; set; }
    }
}
