namespace GxPlayerStore.Models.Output
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents output model for library.
    /// </summary>
    public class LibraryGameOutputModel : GameOutputModel
    {
        /// <summary>
        /// Gets or sets game source.
        /// </summary>
        /// <value>
        /// Game source.
        /// </value>
        public string Source { get; set; }
    }
}
