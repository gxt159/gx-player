namespace GxPlayerStore.Models.Output
{
    /// <summary>
    /// Represents market output model.
    /// </summary>
    public class MarketGameOutputModel : GameOutputModel
    {
        /// <summary>
        /// Gets or sets seller name.
        /// </summary>
        /// <value>
        /// Seller name.
        /// </value>
        public string Seller { get; set; }

        /// <summary>
        /// Gets or sets game cost.
        /// </summary>
        /// <value>
        /// Game cost.
        /// </value>
        public decimal Cost { get; set; }
    }
}
