namespace GxPlayerStore.Models.Output
{
    /// <summary>
    /// Represents output model for store.
    /// </summary>
    public class StoreGameOutputModel : GameOutputModel
    {
        /// <summary>
        /// Gets or sets publisher name.
        /// </summary>
        /// <value>
        /// Publisher name.
        /// </value>
        public string Publisher { get; set; }

        /// <summary>
        /// Gets or sets game cost.
        /// </summary>
        /// <value>
        /// Game cost.
        /// </value>
        public decimal Cost { get; set; }
    }
}
