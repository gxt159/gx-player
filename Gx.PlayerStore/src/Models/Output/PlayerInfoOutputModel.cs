using System;

namespace GxPlayerStore.Models.Output
{
    public class PlayerInfoOutputModel
    {
        /// <summary>
        /// Gets or sets player ID.
        /// </summary>
        /// <value>
        /// Player ID.
        /// </value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets player username.
        /// </summary>
        /// <value>
        /// Player username.
        /// </value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets player money.
        /// </summary>
        /// <value>
        /// Player money.
        /// </value>
        public decimal Money { get; set; }
    }
}
