namespace GxPlayerStore.Models.Output
{
    /// <summary>
    /// Represents output model for game.
    /// </summary>
    public class GameOutputModel
    {
        /// <summary>
        /// Gets or sets game ID.
        /// </summary>
        /// <value>
        /// Game ID.
        /// </value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets game name.
        /// </summary>
        /// <value>
        /// Game name.
        /// </value>
        public string Name { get; set; }
    }
}
