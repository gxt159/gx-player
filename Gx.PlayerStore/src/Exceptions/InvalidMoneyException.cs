namespace GxPlayerStore.Exceptions
{
    using System.Net;

    /// <summary>
    /// 405: Method not allowed. HTTP exception class.
    /// </summary>
    public class InvalidMoneyException : HttpException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidMoneyException"/> class.
        /// </summary>
        /// <param name="detail">Context of the error.</param>
        /// <param name="detailVisible">Flag indicates that detail should be returned in the response.</param>
        public InvalidMoneyException(object detail = null, bool detailVisible = false)
    : base(
        "Invalid money.",
        HttpStatusCode.MethodNotAllowed,
        detail,
        detailVisible)
        {
        }
    }
}
