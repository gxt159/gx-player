namespace GxPlayerStore.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;

    /// <summary>
    /// 403: Access denied.` HTTP exception class.
    /// </summary>
    public class NotEnoughMoneyException : HttpException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotEnoughMoneyException"/> class.
        /// </summary>
        /// <param name="detail">Context of the error.</param>
        /// <param name="detailVisible">Flag indicates that detail should be returned in the response.</param>
        public NotEnoughMoneyException(object detail = null, bool detailVisible = false)
            : base(
                "You have not enough money.",
                HttpStatusCode.Forbidden,
                detail,
                detailVisible)
        {
        }
    }
}
