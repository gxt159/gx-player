namespace GxPlayerStore.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using GxPublisherStore.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;

    /// <summary>
    /// Database context of the application.
    /// </summary>
    public class GXDBContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GXDBContext"/> class.
        /// </summary>
        public GXDBContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GXDBContext"/> class.
        /// </summary>
        /// <param name="configuration">Gathered configuration object.</param>
        public GXDBContext(IConfiguration configuration)
        {
            ServerAddress = configuration["DbConfig:ServerAddress"];
            User = configuration["DbConfig:User"];
            Password = configuration["DbConfig:Password"];
            Port = configuration["DbConfig:Port"];
        }

        /// <summary>
        /// Gets or sets DbSet of <see cref="GameEntity"/> class.
        /// </summary>
        /// <value>
        /// DbSet of <see cref="GameEntity"/> class.
        /// </value>
        public virtual DbSet<GameEntity> Games { get; set; }

        /// <summary>
        /// Gets or sets DbSet of <see cref="LibraryEntity"/> class.
        /// </summary>
        /// <value>
        /// DbSet of <see cref="LibraryEntity"/> class.
        /// </value>
        public virtual DbSet<LibraryEntity> Libraries { get; set; }

        /// <summary>
        /// Gets or sets DbSet of <see cref="MarketEntity"/> class.
        /// </summary>
        /// <value>
        /// DbSet of <see cref="MarketEntity"/> class.
        /// </value>
        public virtual DbSet<MarketEntity> Market { get; set; }

        /// <summary>
        /// Gets or sets DbSet of <see cref="StoreEntity"/> class.
        /// </summary>
        /// <value>
        /// DbSet of <see cref="StoreEntity"/> class.
        /// </value>
        public virtual DbSet<StoreEntity> Store { get; set; }

        /// <summary>
        /// Gets or sets DbSet of <see cref="PublisherEntity"/> class.
        /// </summary>
        /// <value>
        /// DbSet of <see cref="PublisherEntity"/> class.
        /// </value>
        public virtual DbSet<PublisherEntity> Publishers { get; set; }

        /// <summary>
        /// Gets or sets DbSet of <see cref="PlayerEntity"/> class.
        /// </summary>
        /// <value>
        /// DbSet of <see cref="PlayerEntity"/> class.
        /// </value>
        public virtual DbSet<PlayerEntity> Players { get; set; }

        private string User { get; }

        private string Password { get; }

        private string ServerAddress { get; }

        private string Port { get; }

        /// <summary>
        /// Gets games list by player ID.
        /// </summary>
        /// <param name="playerGuid">Represents player ID in database.</param>
        /// <returns>A <see cref="List{GameEntity}"/> representing the asynchronous operation.</returns>
        public async Task<List<LibraryEntity/*GameEntity*/>> GetGamesByPlayerGuid(Guid playerGuid)
        {
            return await Libraries
                .Where(x => x.Player.Id == playerGuid)
                .Include(x => x.Game)
                .Include(x => x.Player)

                // .Select(x => x.Game).
                .ToListAsyncSafe();
        }

        /// <summary>
        /// Gets players' game by games' guid.
        /// </summary>
        /// <param name="libraryGuid">Games' guid.</param>
        /// <returns>Returns founded <see cref="GameEntity"/> or null.</returns>
        public async Task<LibraryEntity> GetGameByGuid(Guid libraryGuid)
        {
            return await Libraries
                .Include(x => x.Player)
                .Include(x => x.Game)
                .SingleOrDefaultAsyncSafe(x => x.Id == libraryGuid);
        }

        /// <summary>
        /// Add market entity to market table.
        /// </summary>
        /// <param name="entity">Represents <see cref="MarketEntity"/>.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task AddLibraryGameToMarket(MarketEntity entity)
        {
            await Market.AddAsync(entity);
            await SaveChangesAsync();
        }

        /// <summary>
        /// Gets game from market without player.
        /// </summary>
        /// <param name="player">Represents <see cref="PlayerEntity"/>.</param>
        /// <returns>A <see cref="List{MarketEntity}"/> representing the asynchronous operation.</returns>
        public async Task<List<MarketEntity>> GetMarketGamesWithoutPlayer(PlayerEntity player)
        {
            return await Market
                .Where(x => x.Library.Player.Id != player.Id)
                .Include(x => x.Library)
                .Include(x => x.Library.Game)
                .Include(x => x.Library.Player)
                .ToListAsyncSafe();
        }

        /// <summary>
        /// Gets game from market with player.
        /// </summary>
        /// <param name="player">Represents <see cref="PlayerEntity"/>.</param>
        /// <returns>A <see cref="List{PlayerEntity}"/> representing the asynchronous operation.</returns>
        public async Task<List<MarketEntity>> GetMarketGamesWithPlayer(PlayerEntity player)
        {
            return await Market
                .Where(x => x.Library.Player.Id == player.Id)
                .Include(x => x.Library)
                .Include(x => x.Library.Game)
                .Include(x => x.Library.Player)
                .ToListAsyncSafe();
        }

        /// <summary>
        /// Gets games list from store.
        /// </summary>
        /// <returns>A <see cref="List{StoreEntity}"/> representing the asynchronous operation.</returns>
        public async Task<List<StoreEntity>> GetAllStoreGames()
        {
            return await Store.Include(x => x.Game).ToListAsyncSafe();
        }

        /// <summary>
        /// Gets market entity by market ID.
        /// </summary>
        /// <param name="guid">Represents market ID.</param>
        /// <returns>A <see cref="MarketEntity"/> representing the asynchronous operation.</returns>
        public async Task<MarketEntity> GetMarketByGuid(Guid guid)
        {
            return await Market.Include(x => x.Library).SingleOrDefaultAsyncSafe(x => x.Id == guid);
        }

        /// <summary>
        /// Gets player entity by player ID.
        /// </summary>
        /// <param name="guid">Represents player ID.</param>
        /// <returns>A <see cref="PlayerEntity"/> representing the asynchronous operation.</returns>
        public async Task<PlayerEntity> GetPlayerByGuid(Guid guid)
        {
            return await Players.SingleOrDefaultAsyncSafe(x => x.Id == guid);
        }

        /// <summary>
        /// Adds library entity to library table.
        /// </summary>
        /// <param name="libraryGame">Represents library entity.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task AddGameToLibrary(LibraryEntity libraryGame)
        {
            await Libraries.AddAsync(libraryGame);
            await SaveChangesAsync();
        }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql($"Server={ServerAddress};Port={Port};Database=gx-db;Username={User};Password={Password}");
        }
    }
}
