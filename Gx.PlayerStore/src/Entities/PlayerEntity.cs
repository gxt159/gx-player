namespace GxPlayerStore.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents player entity in database.
    /// </summary>
    public class PlayerEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerEntity"/> class.
        /// </summary>
        public PlayerEntity()
        {
            Libraries = new List<LibraryEntity>();
        }

        /// <summary>
        /// Gets or sets player ID.
        /// </summary>
        /// <value>
        /// Player ID.
        /// </value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets player username.
        /// </summary>
        /// <value>
        /// Player username.
        /// </value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets player money.
        /// </summary>
        /// <value>
        /// Player money.
        /// </value>
        public decimal Money { get; set; }

        /// <summary>
        /// Gets or sets library collection.
        /// </summary>
        /// <value>
        /// Library collection.
        /// </value>
        public virtual ICollection<LibraryEntity> Libraries { get; set; }
    }
}
