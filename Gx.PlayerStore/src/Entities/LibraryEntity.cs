namespace GxPlayerStore.Entities
{
    using System;

    /// <summary>
    /// Represents library entity in database.
    /// </summary>
    public class LibraryEntity
    {
        /// <summary>
        /// Gets or sets library ID in database.
        /// </summary>
        /// <value>
        /// Library ID in database.
        /// </value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets player foreign key.
        /// </summary>
        /// <value>
        /// Player foreign key.
        /// </value>
        public Guid PlayerId { get; set; }

        /// <summary>
        /// Gets or sets player relation.
        /// </summary>
        /// <value>
        /// Player relation.
        /// </value>
        public virtual PlayerEntity Player { get; set; }

        /// <summary>
        /// Gets or sets game foreign key.
        /// </summary>
        /// <value>
        /// Game foreign key.
        /// </value>
        public Guid GameId { get; set; }

        /// <summary>
        /// Gets or sets game relation.
        /// </summary>
        /// <value>
        /// Game relation.
        /// </value>
        public virtual GameEntity Game { get; set; }
    }
}
