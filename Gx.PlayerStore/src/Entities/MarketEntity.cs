namespace GxPlayerStore.Entities
{
    using System;

    /// <summary>
    /// Represemts market entity in database.
    /// </summary>
    public class MarketEntity
    {
        /// <summary>
        /// Gets or sets market ID in database.
        /// </summary>
        /// <value>
        /// Market ID in database.
        /// </value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets library foreign key.
        /// </summary>
        /// <value>
        /// Library foreign key.
        /// </value>
        public Guid LibraryId { get; set; }

        /// <summary>
        /// Gets or sets library relation.
        /// </summary>
        /// <value>
        /// Library relation.
        /// </value>
        public virtual LibraryEntity Library { get; set; }

        /// <summary>
        /// Gets or sets game cost in market.
        /// </summary>
        /// <value>
        /// Game cost in market.
        /// </value>
        public decimal Cost { get; set; }
    }
}
