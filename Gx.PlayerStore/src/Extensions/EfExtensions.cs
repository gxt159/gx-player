namespace GxPublisherStore.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Extension class for entity framework.
    /// </summary>
    public static class EfExtensions
    {
        /// <summary>
        /// Transforms queryable object to async list safely.
        /// </summary>
        /// <typeparam name="TSource">Database set.</typeparam>
        /// <param name="source">Queryable source.</param>
        /// <returns>Returns list or raise <see cref="ArgumentNullException"/>.</returns>
        public static Task<List<TSource>> ToListAsyncSafe<TSource>(this IQueryable<TSource> source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (!(source is IDbAsyncEnumerable<TSource>))
            {
                return Task.FromResult(source.ToList());
            }

            return source.ToListAsync();
        }

        /// <summary>
        /// Searches element in database.
        /// </summary>
        /// <typeparam name="TSource">Database set.</typeparam>
        /// <param name="source">Queryable source.</param>
        /// <param name="predicate">Some user's predicate.</param>
        /// <returns>Returns element of database or raise <see cref="ArgumentNullException"/>.</returns>
        public static Task<TSource> SingleOrDefaultAsyncSafe<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (!(source is IDbAsyncEnumerable<TSource>))
            {
                return Task.FromResult(source.SingleOrDefault(predicate));
            }

            return source.SingleOrDefaultAsync(predicate);
        }
    }
}
