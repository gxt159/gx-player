namespace GxPlayerStore.Services
{
    using System;
    using System.Threading.Tasks;
    using GxPlayerStore.Entities;
    using GxPlayerStore.Exceptions;

    /// <summary>
    /// Player service.
    /// </summary>
    public class PlayerService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerService"/> class.
        /// </summary>
        /// <param name="context">Db context.</param>
        public PlayerService(GXDBContext context)
        {
            DbContext = context;
        }

        private GXDBContext DbContext { get; }

        /// <summary>
        /// Gets Player by Id.
        /// </summary>
        /// <param name="id">Player id.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        public async Task<PlayerEntity> GetPlayerEntity(string id)
        {
            var isGuid = Guid.TryParse(id, out var guid);

            if (!isGuid)
            {
                throw new NotGuidException();
            }

            return await DbContext.GetPlayerByGuid(guid);
        }
    }
}
