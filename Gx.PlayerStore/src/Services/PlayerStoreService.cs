namespace GxPlayerStore.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using GxPlayerStore.Entities;
    using GxPlayerStore.Exceptions;
    using GxPlayerStore.Models.Input;
    using GxPlayerStore.Models.Output;

    /// <summary>
    /// Represents API for player actions in store.
    /// </summary>
    public class PlayerStoreService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerStoreService"/> class.
        /// </summary>
        /// <param name="context">Represents database context.</param>
        public PlayerStoreService(GXDBContext context)
        {
            Context = context;
        }

        private GXDBContext Context
        {
            get;
        }

        /// <summary>
        /// Gets games list from store.
        /// </summary>
        /// <returns>A <see cref="List{StoreGameOutputModel}"/> representing the asynchronous operation.</returns>
        public virtual async Task<List<StoreGameOutputModel>> GetStoreGames()
        {
            var games = await Context.GetAllStoreGames();
            return games.Select(x => new StoreGameOutputModel
            {
                Id = x.Id.ToString(),
                Name = x.Game.Name,

                // TODO: Change publisherId to publisher name.
                Publisher = x.Game.PublisherId.ToString(),
                Cost = x.Cost,
            }).ToList();
        }

        /// <summary>
        /// Buys specific game from store.
        /// </summary>
        /// <param name="playerModel">Represents <see cref="PlayerInputModel"/>.</param>
        /// <param name="storeModel">Represents <see cref="StoreInputModel"/>.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public virtual async Task BuyGameFromStore(PlayerInputModel playerModel, StoreInputModel storeModel)
        {
            var games = await Context.GetAllStoreGames();
            var game = games.SingleOrDefault(x => x.Id.ToString() == storeModel.Id);
            var isGuid = Guid.TryParse(playerModel.Id, out var guid);

            if (!isGuid)
            {
                throw new NotGuidException();
            }

            var playerEntity = await Context.GetPlayerByGuid(guid);

            if (playerEntity.Money < game.Cost)
            {
                throw new NotEnoughMoneyException();
            }

            playerEntity.Money -= game.Cost;

            var libraryEntity = new LibraryEntity
            {
                Id = default,
                Game = game.Game,
                Player = playerEntity,
            };

            await Context.AddGameToLibrary(libraryEntity);
        }
    }
}
