namespace GxPlayerStore.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using GxPlayerStore.Entities;
    using GxPlayerStore.Exceptions;
    using GxPlayerStore.Models.Input;
    using GxPlayerStore.Models.Input.Market;
    using GxPlayerStore.Models.Output;

    /// <summary>
    /// Represents API for player actions in market.
    /// </summary>
    public class PlayerMarketService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerMarketService"/> class.
        /// </summary>
        /// <param name="context">Represents context of database.</param>
        public PlayerMarketService(GXDBContext context)
        {
            Context = context;
        }

        private GXDBContext Context
        {
            get;
        }

        /// <summary>
        /// Gets dictionary with player and others games.
        /// </summary>
        /// <param name="player">Represents <see cref="PlayerInputModel"/>.</param>
        /// <returns>A string and <see cref="List{MarketGameOutputModel}"/> dictionary representing the asynchronous operation.</returns>
        public virtual async Task<Dictionary<string, List<MarketGameOutputModel>>> GetMarketGames(PlayerInputModel player)
        {
            var isGuid = Guid.TryParse(player.Id, out var guid);

            if (!isGuid)
            {
                throw new NotGuidException();
            }

            var playerEntity = new PlayerEntity
            {
                Id = guid,
            };

            var gamesWithoutPlayer = (await Context.GetMarketGamesWithoutPlayer(playerEntity))
                .Select(x => new MarketGameOutputModel
                {
                    Id = x.Id.ToString(),
                    Name = x.Library.Game.Name,
                    Seller = x.Library.Player.Username,
                    Cost = x.Cost,
                }).ToList();
            var gamesWithPlayer = (await Context.GetMarketGamesWithPlayer(playerEntity))
                .Select(x => new MarketGameOutputModel
                {
                    Id = x.Id.ToString(),
                    Name = x.Library.Game.Name,
                    Seller = x.Library.Player.Username,
                    Cost = x.Cost,
                }).ToList();
            return new Dictionary<string, List<MarketGameOutputModel>>
            {
                { "Player", gamesWithPlayer },
                { "Others", gamesWithoutPlayer },
            };
        }

        /// <summary>
        /// Buys specific game from market.
        /// </summary>
        /// <param name="playerModel">Represents <see cref="PlayerInputModel"/>.</param>
        /// <param name="marketModel">Represents <see cref="MarketGameInputModel"/>.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public virtual async Task BuyGameFromMarket(PlayerInputModel playerModel, MarketGameInputModel marketModel)
        {
            var isGuid = Guid.TryParse(marketModel.Id, out var guid);

            if (!isGuid)
            {
                throw new NotGuidException();
            }

            var gameMarket = await Context.GetMarketByGuid(guid);

            isGuid = Guid.TryParse(playerModel.Id, out guid);

            if (!isGuid)
            {
                throw new NotGuidException();
            }

            var playerEntity = await Context.GetPlayerByGuid(guid);

            var money = playerEntity.Money;
            //var money = 0;
            if (money < gameMarket.Cost)
            {
                throw new NotEnoughMoneyException();
            }

            // TODO: Remove all references to game in market after buying.
            var library = await Context.GetGameByGuid(gameMarket.Library.Id);
            library.Player.Money += gameMarket.Cost;
            playerEntity.Money -= gameMarket.Cost;
            library.Player = playerEntity;
            Context.Market.Remove(gameMarket);
            await Context.SaveChangesAsync();
        }
    }
}
