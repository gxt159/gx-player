namespace GxPlayerStore.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using GxPlayerStore.Entities;
    using GxPlayerStore.Exceptions;
    using GxPlayerStore.Models.Input;
    using GxPlayerStore.Models.Input.Library;
    using GxPlayerStore.Models.Input.Market;
    using GxPlayerStore.Models.Output;

    /// <summary>
    /// Represents API for player actions in library.
    /// </summary>
    public class PlayerLibraryService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerLibraryService"/> class.
        /// </summary>
        /// <param name="context">Represents context of database.</param>
        public PlayerLibraryService(GXDBContext context)
        {
            Context = context;
        }

        private GXDBContext Context { get; }

        /// <summary>
        /// Gets library list.
        /// </summary>
        /// <param name="player">Represents <see cref="PlayerInputModel"/>.</param>
        /// <returns>A <see cref="List{LibraryGameOutputModel}"/> representing the asynchronous operation.</returns>
        public virtual async Task<List<LibraryGameOutputModel>> GetLibraryGames(PlayerInputModel player)
        {
            var isGuid = Guid.TryParse(player.Id, out var guid);

            if (!isGuid)
            {
                throw new NotGuidException();
            }

            var games = await Context.GetGamesByPlayerGuid(guid);
            return games.Select(x => new LibraryGameOutputModel
            {
                Id = x.Id.ToString(),

                // Name = x.Name,
                Name = x.Game.Name,
            }).ToList();
        }

        /// <summary>
        /// Gets specific game from library.
        /// </summary>
        /// <param name="player">Represents <see cref="PlayerInputModel"/>.</param>
        /// <param name="library">Represents <see cref="GameLibraryInputModel"/>.</param>
        /// <returns>A <see cref="LibraryGameOutputModel"/> representing the asynchronous operation.</returns>
        public virtual async Task<LibraryGameOutputModel> GetGame(PlayerInputModel player, GameLibraryInputModel library)
        {
            var isGuid = Guid.TryParse(library.Id, out var guid);

            if (!isGuid)
            {
                throw new NotGuidException();
            }

            var game = await Context.GetGameByGuid(guid);

            if (game == null)
            {
                throw new NotExistingGameException();
            }

            return new LibraryGameOutputModel
            {
                Id = game.Id.ToString(),
                Name = game.Game.Name,
                Source = game.Game.Source,
            };
        }

        /// <summary>
        /// Sells specific game from library.
        /// </summary>
        /// <param name="player">Represents <see cref="PlayerInputModel"/>.</param>
        /// <param name="model">Represents <see cref="MarketSellGameInputModel"/>.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public virtual async Task SellGame(PlayerInputModel player, MarketSellGameInputModel model)
        {
            var library = new GameLibraryInputModel
            {
                Id = model.LibraryGameId,
            };

            var game = await Context.GetGameByGuid(Guid.Parse(library.Id));

            if (model.Cost < 0)
            {
                throw new InvalidMoneyException();
            }

            // TODO: Maybe make a rule class for cost.
            var market = new MarketEntity
            {
                Id = default,
                Library = game,
                Cost = model.Cost,
            };
            await Context.AddLibraryGameToMarket(market);
        }
    }
}
