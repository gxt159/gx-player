namespace GxPlayerStore.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using GxPlayerStore.Exceptions;
    using GxPlayerStore.Models.Input;
    using GxPlayerStore.Models.Input.Market;
    using GxPlayerStore.Models.Output;
    using GxPlayerStore.Services;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Represents controller for player actions in library.
    /// </summary>
    [ApiController]
    [Authorize]
    public class PlayerLibraryController : ControllerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerLibraryController"/> class.
        /// </summary>
        /// <param name="playerService">API for player actions in library.</param>
        public PlayerLibraryController(PlayerLibraryService playerService)
        {
            PlayerService = playerService;
        }

        private PlayerLibraryService PlayerService { get; }

        /// <summary>
        /// Gets player library games.
        /// </summary>
        /// <param name="id">Represents player ID.</param>
        /// <returns>A <see cref="LibraryGameOutputModel"/> list representing the asynchronous operation.</returns>
        /// <response code="200">Player has got library games successfully.</response>
        /// <response code="403">Player tries to get library games from other player.</response>
        [HttpGet("{id}/library/games")]
        public async Task<List<LibraryGameOutputModel>> GetLibraryGames([FromRoute] string id)
        {
            var playerId = User.Claims.First(claim => claim.Type == ClaimTypes.NameIdentifier).Value;

            if (id != playerId)
            {
                throw new ForbiddenException();
            }

            var player = new PlayerInputModel
            {
                Id = playerId,
            };

            return await PlayerService.GetLibraryGames(player);
        }

        /// <summary>
        /// Sells game from library to market.
        /// </summary>
        /// <param name="id">Represents player ID.</param>
        /// <param name="model">Represents model with player and game cost.</param>
        /// <returns>A <see cref="LibraryGameOutputModel"/> list representing the asynchronous operation.</returns>
        /// <response code="200">Player has sold game successfully.</response>
        /// <response code="403">Player tries to sell game from other library.</response>
        [HttpPost("{id}/library/games/sell")]
        public async Task<List<LibraryGameOutputModel>> SellLibraryGame([FromRoute] string id, [FromBody] MarketSellGameInputModel model)
        {
            var playerId = HttpContext.User.Claims.First(claim => claim.Type == ClaimTypes.NameIdentifier).Value;

            if (id != playerId)
            {
                throw new ForbiddenException();
            }

            var player = new PlayerInputModel
            {
                Id = playerId,
            };

            await PlayerService.SellGame(player, model);
            return await PlayerService.GetLibraryGames(player);
        }
    }
}
