namespace GxPlayerStore.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using GxPlayerStore.Models.Input;
    using GxPlayerStore.Models.Output;
    using GxPlayerStore.Services;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Represents controller for player actions in store.
    /// </summary>
    [ApiController]
    [Authorize]
    public class PlayerStoreController : ControllerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerStoreController"/> class.
        /// </summary>
        /// <param name="playerService">API for player actions in store.</param>
        public PlayerStoreController(PlayerStoreService playerService)
        {
            PlayerService = playerService;
        }

        private PlayerStoreService PlayerService { get; }

        /// <summary>
        /// Gets games list in store.
        /// </summary>
        /// <returns>A <see cref="List{StoreGameOutputModel}"/> representing the asynchronous operation.</returns>
        [HttpGet("store/games")]
        public async Task<List<StoreGameOutputModel>> GetStoreGames()
        {
            return await PlayerService.GetStoreGames();
        }

        /// <summary>
        /// Buys game from store.
        /// </summary>
        /// <param name="id">Represents store ID in store.</param>
        /// <returns>A <see cref="List{StoreGameOutputModel}"/> representing the asynchronous operation.</returns>
        [HttpPost("store/games/{id}/buy")]
        public async Task<List<StoreGameOutputModel>> BuyStoreGame([FromRoute] string id)
        {
            var playerId = User.Claims.First(claim => claim.Type == ClaimTypes.NameIdentifier).Value;

            var player = new PlayerInputModel
            {
                Id = playerId,
            };

            var store = new StoreInputModel
            {
                Id = id,
            };
            await PlayerService.BuyGameFromStore(player, store);
            return await PlayerService.GetStoreGames();
        }
    }
}
