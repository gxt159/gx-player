namespace GxPlayerStore.Controllers
{
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using GxPlayerStore.Exceptions;
    using GxPlayerStore.Models.Input;
    using GxPlayerStore.Models.Output;
    using GxPlayerStore.Services;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Player info controller.
    /// </summary>
    [ApiController]
    [Authorize]
    public class PlayerInfoController : ControllerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerInfoController"/> class.
        /// </summary>
        /// <param name="playerService">Player service.</param>
        public PlayerInfoController(PlayerService playerService)
        {
            PlayerService = playerService;
        }

        private PlayerService PlayerService { get; }

        /// <summary>
        /// Gets player info.
        /// </summary>
        /// <param name="id">Player id.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        [HttpGet("{id}/info")]
        public async Task<PlayerInfoOutputModel> GetPlayerInfo([FromRoute] string id)
        {
            var playerId = User.Claims.First(claim => claim.Type == ClaimTypes.NameIdentifier).Value;

            if (id != playerId)
            {
                throw new ForbiddenException();
            }

            var playerEntity = await PlayerService.GetPlayerEntity(id);

            return new PlayerInfoOutputModel
            {
                Id = playerEntity.Id,
                Username = playerEntity.Username,
                Money = playerEntity.Money,
            };
        }
    }
}
