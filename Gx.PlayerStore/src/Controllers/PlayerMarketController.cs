namespace GxPlayerStore.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using GxPlayerStore.Models.Input;
    using GxPlayerStore.Models.Input.Market;
    using GxPlayerStore.Models.Output;
    using GxPlayerStore.Services;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Represents controller for player actions in market.
    /// </summary>
    [ApiController]
    public class PlayerMarketController : ControllerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerMarketController"/> class.
        /// </summary>
        /// <param name="playerService">API for player actions in market.</param>
        public PlayerMarketController(PlayerMarketService playerService)
        {
            PlayerService = playerService;
        }

        private PlayerMarketService PlayerService { get; }

        /// <summary>
        /// Gets dictionary with player and others games in market.
        /// </summary>
        /// <returns>A string and <see cref="List{MarketGameOutputModel}"/> dictionary representing the asynchronous operation.</returns>
        /// <response code="200">Player has got dictionary with games.</response>
        [HttpGet("{id}/market/games")]
        public async Task<Dictionary<string, List<MarketGameOutputModel>>> GetMarketGames()
        {
            var playerId = HttpContext.User.Claims.First(claim => claim.Type == ClaimTypes.NameIdentifier).Value;

            var player = new PlayerInputModel
            {
                Id = playerId,
            };

            return await PlayerService.GetMarketGames(player);
        }

        /// <summary>
        /// Gets dictionary with player and others games in market.
        /// </summary>
        /// <param name="id">Represents market ID in market.</param>
        /// <returns>A string and <see cref="List{MarketGameOutputModel}"/> dictionary representing the asynchronous operation.</returns>
        /// <response code="200">Player has bought game from market successfully.</response>
        [HttpPost("market/games/{id}/buy")]
        public async Task<Dictionary<string, List<MarketGameOutputModel>>> BuyMarketGame([FromRoute] string id)
        {
            var playerId = HttpContext.User.Claims.First(claim => claim.Type == ClaimTypes.NameIdentifier).Value;

            var player = new PlayerInputModel
            {
                Id = playerId,
            };
            var marketGame = new MarketGameInputModel
            {
                Id = id,
            };
            await PlayerService.BuyGameFromMarket(player, marketGame);
            return await PlayerService.GetMarketGames(player);
        }
    }
}
