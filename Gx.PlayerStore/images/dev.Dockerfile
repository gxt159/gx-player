ARG DOTNET_CORE_VERSION=3.1

FROM mcr.microsoft.com/dotnet/core/sdk:$DOTNET_CORE_VERSION

WORKDIR /app

ENTRYPOINT [ "dotnet", "watch", "run" ]
