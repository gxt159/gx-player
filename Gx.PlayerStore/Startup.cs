namespace GxPlayerStore
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using GxPlayerStore.Configuration;
    using GxPlayerStore.Entities;
    using GxPlayerStore.Services;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.IdentityModel.Tokens;
    using Microsoft.OpenApi.Models;
    using Org.BouncyCastle.Crypto;
    using Org.BouncyCastle.Crypto.Generators;
    using Org.BouncyCastle.Crypto.Parameters;
    using Org.BouncyCastle.OpenSsl;
    using Org.BouncyCastle.Security;
    using ScottBrady.IdentityModel.Tokens;

    /// <summary>
    /// Class, responsible for configuring the app, tuning of services and request pipes.
    /// </summary>
    public class Startup
    {
        private const string PublicCorsPolicyName = "PublicPolicy";

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="env">Object, responsible for the application environment (get and interact with it).</param>
        /// <param name="configuration">Gathered configuration object.</param>
        public Startup(IWebHostEnvironment env, IConfiguration configuration)
        {
            CurrentEnvironment = env;
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        private IWebHostEnvironment CurrentEnvironment { get; }

        /// <summary>
        /// Register services, which are necessary for the app.<br/>
        /// Methods have signature "Add[ServiceName]".
        /// </summary>
        /// <param name="services">Collection of services to add.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(PublicCorsPolicyName, builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            services.AddDbContext<GXDBContext>();

            var (publicKey, privateKey) = ReadKeys();

            var jwtConfig = Configuration.GetSection("JwtConfiguration").Get<JwtConfiguration>();

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtConfig.Issuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = publicKey,
                ValidAudience = jwtConfig.Audience,
                ValidateAudience = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.FromMinutes(1),
            };

            services.AddAuthentication("Bearer").AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.RequireHttpsMetadata = false;
                jwtBearerOptions.SaveToken = true;
                jwtBearerOptions.TokenValidationParameters = tokenValidationParameters;
            });

            services.AddScoped<PlayerStoreService>();
            services.AddScoped<PlayerMarketService>();
            services.AddScoped<PlayerLibraryService>();
            services.AddScoped<PlayerService>();

            services.AddControllers();

            var appVersion = Configuration["AppVersion"];
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(appVersion, new OpenApiInfo
                {
                    Version = appVersion,
                    Title = "Gx.Player service API",
                });

                // Set the comments path for the Swagger JSON and UI.
                // Source: https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-3.1&tabs=visual-studio#xml-comments
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        /// <summary>
        /// Configures, how the app will proceed requests.
        /// </summary>
        /// <param name="app">Object, responsible for setting components of the app.<br/>
        /// Methods have signature "Use[ComponentName]".</param>
        /// <param name="env">Object, responsible for the application environment (get and interact with it).</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<GXDBContext>();
                context.Database.Migrate();
            }

            if (env.IsDevelopment())
            {
                app.UseExceptionHandler("/error/dev");
            }
            else
            {
                app.UseExceptionHandler("/error");
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            app.UseCors(PublicCorsPolicyName);

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/{Configuration["AppVersion"]}/swagger.json", "Gx.Player service API");
            });

            app.UseDeveloperExceptionPage();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private static (Ed25519PublicKeyParameters publicKeyParameter, Ed25519PrivateKeyParameters privateKeyParameter) GenerateTempKeys()
        {
            var keyPairGenerator = new Ed25519KeyPairGenerator();
            keyPairGenerator.Init(new Ed25519KeyGenerationParameters(new SecureRandom()));
            var keyPair = keyPairGenerator.GenerateKeyPair();

            var privateKeyParameter = (Ed25519PrivateKeyParameters)keyPair.Private;
            var publicKeyParameter = (Ed25519PublicKeyParameters)keyPair.Public;

            return (publicKeyParameter, privateKeyParameter);
        }

        private static AsymmetricKeyParameter ReadAsymmetricKeyParameterFromBase64String(string base64String)
        {
            var decodedPemString = Convert.FromBase64String(base64String);
            var stringReader = new StringReader(Encoding.UTF8.GetString(decodedPemString));
            var pemReader = new PemReader(stringReader);
            var keyParameter = (AsymmetricKeyParameter)pemReader.ReadObject();
            return keyParameter;
        }

        private (AsymmetricSecurityKey, AsymmetricSecurityKey) ReadKeys()
        {
            Ed25519PublicKeyParameters publicKeyParameter;
            Ed25519PrivateKeyParameters privateKeyParameter;

            publicKeyParameter = ReadAsymmetricKeyParameterFromBase64String(Configuration["GxUserPublicKey"]) as Ed25519PublicKeyParameters;
            privateKeyParameter = ReadAsymmetricKeyParameterFromBase64String(Configuration["GxUserPrivateKey"]) as Ed25519PrivateKeyParameters;
            /*if (CurrentEnvironment.IsProduction())
            {
                publicKeyParameter = ReadAsymmetricKeyParameterFromBase64String(Configuration["GxUserPublicKey"]) as Ed25519PublicKeyParameters;
                privateKeyParameter = ReadAsymmetricKeyParameterFromBase64String(Configuration["GxUserPrivateKey"]) as Ed25519PrivateKeyParameters;
            }
            else
            {
                (publicKeyParameter, privateKeyParameter) = GenerateTempKeys();
            }*/

            var publicKey = new EdDsaSecurityKey(publicKeyParameter);
            var privateKey = new EdDsaSecurityKey(privateKeyParameter);

            return (publicKey, privateKey);
        }
    }
}
