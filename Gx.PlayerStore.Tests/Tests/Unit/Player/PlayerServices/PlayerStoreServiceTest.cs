using GxPlayerStore.Entities;
using GxPlayerStore.Exceptions;
using GxPlayerStore.Models.Input;
using GxPlayerStore.Models.Output;
using GxPlayerStore.Services;
using GxPlayerStore.Tests.Extensions;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GxPlayerStore.Tests.Tests.Unit.Player.PlayerServices
{
    public class PlayerStoreServiceTest
    {
        [Fact]
        public async Task When_GetStoreGames_ReturnStoreGames()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                Name = "Test",
                PublisherId = publisherEntity.Id,
                Publisher = publisherEntity,
                Source = "Test",
            };

            var storeEntity = new StoreEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                GameId = gameEntity.Id,
                Cost = 250,
            };

            var store = new List<StoreEntity>
            {
                storeEntity,
            };

            var expected = new List<StoreGameOutputModel>
            {
                new StoreGameOutputModel
                {
                    Id = storeEntity.Id.ToString(),
                    Name = gameEntity.Name,
                    Cost = storeEntity.Cost,
                    Publisher = gameEntity.PublisherId.ToString(),
                },
            };

            var mockDbContext = new Mock<GXDBContext>();
            var mockStoreDbSet = QueryableExtension.GetQueryableMockDbSet(store);

            // Arrange behavior.
            mockDbContext.Setup(x => x.Store).Returns(mockStoreDbSet);

            // Arrange SUT.
            var playerService = new PlayerStoreService(mockDbContext.Object);

            // Act.
            var actual = await playerService.GetStoreGames();

            // Assert.
            Assert.Equal(expected.Count, actual.Count);
        }

        [Fact]
        public async Task When_BuyGameFromStoreWithNotEnoughMoney_RaiseNotEnoughMoneyException()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var playerEntity = new PlayerEntity
            {
                Id = Guid.NewGuid(),
                Money = 0,
                Username = "Bum",
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                Name = "Test",
                PublisherId = publisherEntity.Id,
                Publisher = publisherEntity,
                Source = "Test",
            };

            var storeEntity = new StoreEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                GameId = gameEntity.Id,
                Cost = 250,
            };

            var players = new List<PlayerEntity>
            {
                playerEntity,
            };

            var store = new List<StoreEntity>
            {
                storeEntity,
            };

            var playerModel = new PlayerInputModel
            {
                Id = playerEntity.Id.ToString(),
            };

            var storeModel = new StoreInputModel
            {
                Id = storeEntity.Id.ToString(),
            };

            var mockDbContext = new Mock<GXDBContext>();
            var mockStoreDbSet = QueryableExtension.GetQueryableMockDbSet(store);
            var mockPlayersDbSet = QueryableExtension.GetQueryableMockDbSet(players);

            // Arrange behavior.
            mockDbContext.Setup(x => x.Store).Returns(mockStoreDbSet);
            mockDbContext.Setup(x => x.Players).Returns(mockPlayersDbSet);

            // Arrange SUT.
            var playerService = new PlayerStoreService(mockDbContext.Object);

            // Assert.
            await Assert.ThrowsAsync<NotEnoughMoneyException>(() => playerService.BuyGameFromStore(playerModel, storeModel));
        }

        [Fact]
        public async Task When_BuyGameFromStoreWithEnoughMoney_Return()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var playerEntity = new PlayerEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                Username = "Bum",
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                Name = "Test",
                PublisherId = publisherEntity.Id,
                Publisher = publisherEntity,
                Source = "Test",
            };

            var storeEntity = new StoreEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                GameId = gameEntity.Id,
                Cost = 250,
            };

            var players = new List<PlayerEntity>
            {
                playerEntity,
            };

            var store = new List<StoreEntity>
            {
                storeEntity,
            };

            var libraries = new List<LibraryEntity>();

            var playerModel = new PlayerInputModel
            {
                Id = playerEntity.Id.ToString(),
            };

            var storeModel = new StoreInputModel
            {
                Id = storeEntity.Id.ToString(),
            };

            var mockDbContext = new Mock<GXDBContext>();
            var mockStoreDbSet = QueryableExtension.GetQueryableMockDbSet(store);
            var mockPlayersDbSet = QueryableExtension.GetQueryableMockDbSet(players);
            var mockLibrariesDbSet = QueryableExtension.GetQueryableMockDbSet(libraries);

            var expected = 250;

            // Arrange behavior.
            mockDbContext.Setup(x => x.Store).Returns(mockStoreDbSet);
            mockDbContext.Setup(x => x.Players).Returns(mockPlayersDbSet);
            mockDbContext.Setup(x => x.Libraries).Returns(mockLibrariesDbSet);

            // Arrange SUT.
            var playerService = new PlayerStoreService(mockDbContext.Object);

            // Act.
            await playerService.BuyGameFromStore(playerModel, storeModel);
            var actual = playerEntity.Money;

            // Assert.
            Assert.Equal(expected, actual);
        }
    }
}
