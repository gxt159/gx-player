using GxPlayerStore.Entities;
using GxPlayerStore.Exceptions;
using GxPlayerStore.Models.Input;
using GxPlayerStore.Models.Input.Market;
using GxPlayerStore.Models.Output;
using GxPlayerStore.Services;
using GxPlayerStore.Tests.Extensions;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GxPlayerStore.Tests.Tests.Unit.Player.PlayerServices
{
    public class PlayerMarketServiceTest
    {
        /*[Fact]
        public async Task When_GetMarketGames_ReturnDictionaryWithPlayer()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var playerEntity = new PlayerEntity
            {
                Id = Guid.Parse("da9182fe-8bad-4739-a706-c808d1ea6ee7"),
                Money = 500,
                Username = "Test",
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                Name = "Test",
                Source = "Test",
                PublisherId = publisherEntity.Id,
                Publisher = publisherEntity,
            };

            var libraryEntity = new LibraryEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                Player = playerEntity,
                GameId = gameEntity.Id,
                PlayerId = playerEntity.Id,
            };

            var marketEntity = new MarketEntity
            {
                Id = Guid.NewGuid(),
                Cost = 150,
                Library = libraryEntity,
                LibraryId = libraryEntity.Id,
            };

            var playerModel = new PlayerInputModel
            {
                Id = playerEntity.Id.ToString(),
            };

            var libraries = new List<LibraryEntity>
            {
                libraryEntity,
            };

            var market = new List<MarketEntity>
            {
                marketEntity,
            };

            var expected = new Dictionary<string, List<MarketGameOutputModel>>
            {
                {
                    "Player", new List<MarketGameOutputModel>
                    {
                                            new MarketGameOutputModel
                    {
                        Id = marketEntity.Id.ToString(),
                        Cost = marketEntity.Cost,
                        Name = marketEntity.Library.Game.Name,
                        Seller = marketEntity.Library.Player.Username,
                    },
                    }

                },
                {
                    "Others", new List<MarketGameOutputModel>()
                },
            };

            var mockDbContext = new Mock<GXDBContext>();
            var mockLibraryDbSet = QueryableExtension.GetQueryableMockDbSet(libraries);
            var mockMarketDbSet = QueryableExtension.GetQueryableMockDbSet(market);

            // Arrange behavior.
            mockDbContext.Setup(x => x.Libraries).Returns(mockLibraryDbSet);
            mockDbContext.Setup(x => x.Market).Returns(mockMarketDbSet);

            // Arrange SUT.
            var playerService = new PlayerMarketService(mockDbContext.Object);

            // Act.
            var actual = await playerService.GetMarketGames(playerModel);

            // Assert.
            Assert.Equal(expected["Player"].Count, actual["Player"].Count);
            Assert.Equal(expected["Others"].Count, actual["Others"].Count);
        }

        [Fact]
        public async Task When_GetMarketGames_ReturnDictionaryWithoutPlayer()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var playerEntity = new PlayerEntity
            {
                Id = Guid.Parse("9f96ee9e-031f-4eb8-a3ba-fd2a4c3be876"),
                Money = 500,
                Username = "Test",
            };

            var playerEntity1 = new PlayerEntity
            {
                Id = Guid.Parse("da9182fe-8bad-4739-a706-c808d1ea6ee7"),
                Money = 500,
                Username = "Aaa",
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                Name = "Test",
                Source = "Test",
                PublisherId = publisherEntity.Id,
                Publisher = publisherEntity,
            };

            var libraryEntity = new LibraryEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                Player = playerEntity,
                GameId = gameEntity.Id,
                PlayerId = playerEntity.Id,
            };

            var marketEntity = new MarketEntity
            {
                Id = Guid.NewGuid(),
                Cost = 150,
                Library = libraryEntity,
                LibraryId = libraryEntity.Id,
            };

            var playerModel = new PlayerInputModel
            {
                Id = playerEntity1.Id.ToString(),
            };

            var libraries = new List<LibraryEntity>
            {
                libraryEntity,
            };

            var market = new List<MarketEntity>
            {
                marketEntity,
            };

            var expected = new Dictionary<string, List<MarketGameOutputModel>>
            {
                {
                    "Player", new List<MarketGameOutputModel>()
                },
                {
                    "Others", new List<MarketGameOutputModel>
                                                            {
                    new MarketGameOutputModel
                    {
                        Id = marketEntity.Id.ToString(),
                        Cost = marketEntity.Cost,
                        Name = marketEntity.Library.Game.Name,
                        Seller = marketEntity.Library.Player.Username,
                    },
                                                            }
                },
            };

            var mockDbContext = new Mock<GXDBContext>();
            var mockLibraryDbSet = QueryableExtension.GetQueryableMockDbSet(libraries);
            var mockMarketDbSet = QueryableExtension.GetQueryableMockDbSet(market);

            // Arrange behavior.
            mockDbContext.Setup(x => x.Libraries).Returns(mockLibraryDbSet);
            mockDbContext.Setup(x => x.Market).Returns(mockMarketDbSet);

            // Arrange SUT.
            var playerService = new PlayerMarketService(mockDbContext.Object);

            // Act.
            var actual = await playerService.GetMarketGames(playerModel);

            // Assert.
            Assert.Equal(expected["Player"].Count, actual["Player"].Count);
            Assert.Equal(expected["Others"].Count, actual["Others"].Count);
        }

        [Fact]
        public async Task When_GetMarketGames_ReturnDictionaryWithGamesWithAndWithoutPlayer()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var playerEntity = new PlayerEntity
            {
                Id = Guid.Parse("9f96ee9e-031f-4eb8-a3ba-fd2a4c3be876"),
                Money = 500,
                Username = "Test",
            };

            var playerEntity1 = new PlayerEntity
            {
                Id = Guid.Parse("da9182fe-8bad-4739-a706-c808d1ea6ee7"),
                Money = 500,
                Username = "Aaa",
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                Name = "Test",
                Source = "Test",
                PublisherId = publisherEntity.Id,
                Publisher = publisherEntity,
            };

            var libraryEntity = new LibraryEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                Player = playerEntity,
                GameId = gameEntity.Id,
                PlayerId = playerEntity.Id,
            };

            var libraryEntity1 = new LibraryEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                Player = playerEntity1,
                GameId = gameEntity.Id,
                PlayerId = playerEntity1.Id,
            };

            var marketEntity = new MarketEntity
            {
                Id = Guid.NewGuid(),
                Cost = 150,
                Library = libraryEntity,
                LibraryId = libraryEntity.Id,
            };

            var marketEntity1 = new MarketEntity
            {
                Id = Guid.NewGuid(),
                Cost = 150,
                Library = libraryEntity1,
                LibraryId = libraryEntity1.Id,
            };

            var playerModel = new PlayerInputModel
            {
                Id = playerEntity1.Id.ToString(),
            };

            var libraries = new List<LibraryEntity>
            {
                libraryEntity,
                libraryEntity1,
            };

            var market = new List<MarketEntity>
            {
                marketEntity,
                marketEntity1,
            };

            var expected = new Dictionary<string, List<MarketGameOutputModel>>
            {
                {
                    "Player", new List<MarketGameOutputModel>
                    {
                        new MarketGameOutputModel
                        {
                            Id = marketEntity1.Id.ToString(),
                            Cost = marketEntity1.Cost,
                            Name = marketEntity1.Library.Game.Name,
                            Seller = marketEntity1.Library.Player.Username,
                        },
                    }
                },
                {
                    "Others", new List<MarketGameOutputModel>
                                                            {
                    new MarketGameOutputModel
                    {
                        Id = marketEntity.Id.ToString(),
                        Cost = marketEntity.Cost,
                        Name = marketEntity.Library.Game.Name,
                        Seller = marketEntity.Library.Player.Username,
                    },
                                                            }
                },
            };

            var mockDbContext = new Mock<GXDBContext>();
            var mockLibraryDbSet = QueryableExtension.GetQueryableMockDbSet(libraries);
            var mockMarketDbSet = QueryableExtension.GetQueryableMockDbSet(market);

            // Arrange behavior.
            mockDbContext.Setup(x => x.Libraries).Returns(mockLibraryDbSet);
            mockDbContext.Setup(x => x.Market).Returns(mockMarketDbSet);

            // Arrange SUT.
            var playerService = new PlayerMarketService(mockDbContext.Object);

            // Act.
            var actual = await playerService.GetMarketGames(playerModel);

            // Assert.
            Assert.Equal(expected["Player"].Count, actual["Player"].Count);
            Assert.Equal(expected["Others"].Count, actual["Others"].Count);
        }

        [Fact]
        public async Task When_GetMarketGamesWithPlayerIdIsNotGuid_RaiseNotGuidException()
        {
            // Arrange.
            var guid = "ABC";
            var playerModel = new PlayerInputModel
            {
                Id = guid,
            };

            var mockDbContext = new Mock<GXDBContext>();

            // Arrange SUT.
            var playerService = new PlayerMarketService(mockDbContext.Object);

            // Assert.
            await Assert.ThrowsAsync<NotGuidException>(() => playerService.GetMarketGames(playerModel));
        }

        [Fact]
        public async Task When_BuyGameFromMarketWithNotEnoughMoney_RaiseNotEnoughMoneyException()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var playerEntity = new PlayerEntity
            {
                Id = Guid.Parse("9f96ee9e-031f-4eb8-a3ba-fd2a4c3be876"),
                Money = 0,
                Username = "Test",
            };

            var playerEntity1 = new PlayerEntity
            {
                Id = Guid.Parse("da9182fe-8bad-4739-a706-c808d1ea6ee7"),
                Money = 0,
                Username = "Aaa",
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                Name = "Test",
                Source = "Test",
                PublisherId = publisherEntity.Id,
                Publisher = publisherEntity,
            };

            var libraryEntity = new LibraryEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                Player = playerEntity,
                GameId = gameEntity.Id,
                PlayerId = playerEntity.Id,
            };

            var marketEntity = new MarketEntity
            {
                Id = Guid.NewGuid(),
                Cost = 150,
                Library = libraryEntity,
                LibraryId = libraryEntity.Id,
            };

            var playerModel = new PlayerInputModel
            {
                Id = playerEntity1.Id.ToString(),
            };

            var marketModel = new MarketGameInputModel
            {
                Id = marketEntity.Id.ToString(),
            };

            var players = new List<PlayerEntity>
            {
                playerEntity,
                playerEntity1,
            };

            var libraries = new List<LibraryEntity>
            {
                libraryEntity,
            };

            var market = new List<MarketEntity>
            {
                marketEntity,
            };

            var mockDbContext = new Mock<GXDBContext>();
            var mockLibraryDbSet = QueryableExtension.GetQueryableMockDbSet(libraries);
            var mockMarketDbSet = QueryableExtension.GetQueryableMockDbSet(market);
            var mockPlayersDbSet = QueryableExtension.GetQueryableMockDbSet(players);

            // Arrange behavior.
            mockDbContext.Setup(x => x.Libraries).Returns(mockLibraryDbSet);
            mockDbContext.Setup(x => x.Market).Returns(mockMarketDbSet);
            mockDbContext.Setup(x => x.Players).Returns(mockPlayersDbSet);

            // Arrange SUT.
            var playerService = new PlayerMarketService(mockDbContext.Object);

            // Assert.
            await Assert.ThrowsAsync<NotEnoughMoneyException>(() => playerService.BuyGameFromMarket(playerModel, marketModel));
        }

        [Fact]
        public async Task When_BuyGameFromMarketWithEnoughMoney_ReturnTask()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var playerEntity = new PlayerEntity
            {
                Id = Guid.Parse("9f96ee9e-031f-4eb8-a3ba-fd2a4c3be876"),
                Money = 500,
                Username = "Test",
            };

            var playerEntity1 = new PlayerEntity
            {
                Id = Guid.Parse("da9182fe-8bad-4739-a706-c808d1ea6ee7"),
                Money = 500,
                Username = "Aaa",
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                Name = "Test",
                Source = "Test",
                PublisherId = publisherEntity.Id,
                Publisher = publisherEntity,
            };

            var libraryEntity = new LibraryEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                Player = playerEntity,
                GameId = gameEntity.Id,
                PlayerId = playerEntity.Id,
            };

            var marketEntity = new MarketEntity
            {
                Id = Guid.NewGuid(),
                Cost = 150,
                Library = libraryEntity,
                LibraryId = libraryEntity.Id,
            };

            var playerModel = new PlayerInputModel
            {
                Id = playerEntity1.Id.ToString(),
            };

            var marketModel = new MarketGameInputModel
            {
                Id = marketEntity.Id.ToString(),
            };

            var players = new List<PlayerEntity>
            {
                playerEntity,
                playerEntity1,
            };

            var libraries = new List<LibraryEntity>
            {
                libraryEntity,
            };

            var market = new List<MarketEntity>
            {
                marketEntity,
            };

            var mockDbContext = new Mock<GXDBContext>();
            var mockLibraryDbSet = QueryableExtension.GetQueryableMockDbSet(libraries);
            var mockMarketDbSet = QueryableExtension.GetQueryableMockDbSet(market);
            var mockPlayersDbSet = QueryableExtension.GetQueryableMockDbSet(players);

            var expected = 500;

            // Arrange behavior.
            mockDbContext.Setup(x => x.Libraries).Returns(mockLibraryDbSet);
            mockDbContext.Setup(x => x.Market).Returns(mockMarketDbSet);
            mockDbContext.Setup(x => x.Players).Returns(mockPlayersDbSet);

            // Arrange SUT.
            var playerService = new PlayerMarketService(mockDbContext.Object);

            // Act.
            await playerService.BuyGameFromMarket(playerModel, marketModel);

            // Assert.
            Assert.Equal(playerEntity1.Money, expected - 150);
            Assert.Equal(playerEntity.Money, expected + 150);
        }*/
    }
}
