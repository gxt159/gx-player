using GxPlayerStore.Entities;
using GxPlayerStore.Exceptions;
using GxPlayerStore.Models.Input;
using GxPlayerStore.Models.Input.Library;
using GxPlayerStore.Models.Input.Market;
using GxPlayerStore.Models.Output;
using GxPlayerStore.Services;
using GxPlayerStore.Tests.Extensions;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace GxPlayerStore.Tests.Tests.Unit.Player.PlayerServices
{
    public class PlayerLibraryServiceTest
    {
        [Fact]
        public async Task When_GetLibraryGamesWithPlayerIdIsNotGuid_RaiseNotGuidException()
        {
            // Arrange.
            var guid = "ABC";
            var playerModel = new PlayerInputModel
            {
                Id = guid,
            };

            var mockDbContext = new Mock<GXDBContext>();

            // Arrange SUT.
            var playerService = new PlayerLibraryService(mockDbContext.Object);

            // Assert.
            await Assert.ThrowsAsync<NotGuidException>(() => playerService.GetLibraryGames(playerModel));
        }

        [Fact]
        public async Task When_GetLibraryGames_ReturnListWithGames()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                PublisherName = "Test",
                Money = 500,
            };

            var playerEntity = new PlayerEntity
            {
                Id = Guid.NewGuid(),
                Username = "Test",
                Money = 500,
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                Name = "Test",
                PublisherId = publisherEntity.Id,
                Publisher = publisherEntity,
                Source = "Test",
            };

            var libraryEntity = new LibraryEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                GameId = gameEntity.Id,
                Player = playerEntity,
                PlayerId = playerEntity.Id,
            };

            var players = new List<PlayerEntity>
            {
                playerEntity,
            };

            var games = new List<GameEntity>
            {
                gameEntity,
            };

            var libraries = new List<LibraryEntity>
            {
                libraryEntity,
            };

            var expected = new List<LibraryGameOutputModel>
            {
                new LibraryGameOutputModel
                {
                    Id = libraryEntity.Id.ToString(),
                    Name = "Test",
                    Source = null,
                },
            };

            var playerModel = new PlayerInputModel
            {
                Id = playerEntity.Id.ToString(),
            };

            var mockDbSet = QueryableExtension.GetQueryableMockDbSet(libraries);
            var mockContext = new Mock<GXDBContext>();

            // Arrange behavior.
            mockContext.Setup(x => x.Libraries).Returns(mockDbSet);

            // Arrange SUT.
            var playerService = new PlayerLibraryService(mockContext.Object);

            // Act.
            var actual = await playerService.GetLibraryGames(playerModel);

            // Assert.
            Assert.Equal(expected.Count, actual.Count);
        }

        [Fact]
        public async Task When_GetLibraryGameWithLibraryIdIsNotGuid_RaiseNotGuidException()
        {
            // Arrange.
            var guid = "ABC";
            var playerModel = new PlayerInputModel
            {
                Id = guid,
            };

            var gameModel = new GameLibraryInputModel
            {
                Id = "ABC",
            };

            var mockDbContext = new Mock<GXDBContext>();

            // Arrange SUT.
            var playerService = new PlayerLibraryService(mockDbContext.Object);

            // Assert.
            await Assert.ThrowsAsync<NotGuidException>(() => playerService.GetGame(playerModel, gameModel));
        }

        [Fact]
        public async Task When_GetLibraryGameWitNonExistingGamehModels_RaiseNotExistingGameException()
        {
            // Arrange.
            var playerEntity = new PlayerEntity
            {
                Id = Guid.NewGuid(),
                Username = "Test",
                Money = 500,
            };

            var libraries = new List<LibraryEntity>
            {
            };

            var playerModel = new PlayerInputModel
            {
                Id = playerEntity.Id.ToString(),
            };

            var gameModel = new GameLibraryInputModel
            {
                Id = Guid.NewGuid().ToString(),
            };

            var mockDbSet = QueryableExtension.GetQueryableMockDbSet(libraries);
            var mockContext = new Mock<GXDBContext>();

            // Arrange behavior.
            mockContext.Setup(x => x.Libraries).Returns(mockDbSet);

            // Arrange SUT.
            var playerService = new PlayerLibraryService(mockContext.Object);

            // Assert.
            await Assert.ThrowsAsync<NotExistingGameException>(() => playerService.GetGame(playerModel, gameModel));
        }

        [Fact]
        public async Task When_GetLibrary_ReturnRequiredGame()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var playerEntity = new PlayerEntity
            {
                Id = Guid.NewGuid(),
                Username = "Test",
                Money = 500,
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                Name = "Test",
                PublisherId = publisherEntity.Id,
                Publisher = publisherEntity,
                Source = "Test",
            };

            var libraryEntity = new LibraryEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                GameId = gameEntity.Id,
                Player = playerEntity,
                PlayerId = playerEntity.Id,
            };

            var players = new List<PlayerEntity>
            {
                playerEntity,
            };

            var games = new List<GameEntity>
            {
                gameEntity,
            };

            var libraries = new List<LibraryEntity>
            {
                libraryEntity,
            };

            var expected = new LibraryGameOutputModel
            {
                Id = libraryEntity.Id.ToString(),
                Name = gameEntity.Name,
                Source = "Test",
            };

            var playerModel = new PlayerInputModel
            {
                Id = playerEntity.Id.ToString(),
            };

            var gameModel = new GameLibraryInputModel
            {
                Id = libraryEntity.Id.ToString(),
            };

            var mockDbSet = QueryableExtension.GetQueryableMockDbSet(libraries);
            var mockContext = new Mock<GXDBContext>();

            // Arrange behavior.
            mockContext.Setup(x => x.Libraries).Returns(mockDbSet);

            // Arrange SUT.
            var playerService = new PlayerLibraryService(mockContext.Object);

            // Act.
            var actual = await playerService.GetGame(playerModel, gameModel);

            // Assert.
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.Source, actual.Source);
        }


        [Fact]
        public async Task When_SellGame_Return()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var playerEntity = new PlayerEntity
            {
                Id = Guid.NewGuid(),
                Username = "Test",
                Money = 500,
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                Name = "Test",
                Publisher = publisherEntity,
                PublisherId = publisherEntity.Id,
                Source = "Test",
            };

            var libraryEntity = new LibraryEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                GameId = gameEntity.Id,
                Player = playerEntity,
                PlayerId = playerEntity.Id,
            };

            var players = new List<PlayerEntity>
            {
                playerEntity,
            };

            var games = new List<GameEntity>
            {
                gameEntity,
            };

            var libraries = new List<LibraryEntity>
            {
                libraryEntity,
            };

            var market = new List<MarketEntity>();

            var expected = new LibraryGameOutputModel
            {
                Id = libraryEntity.Id.ToString(),
                Name = gameEntity.Name,
                Source = "Test",
            };

            var playerModel = new PlayerInputModel
            {
                Id = playerEntity.Id.ToString(),
            };

            var gameModel = new MarketSellGameInputModel
            {
                LibraryGameId = libraryEntity.Id.ToString(),
                Cost = 200,
            };

            var mockLibrariesDbSet = QueryableExtension.GetQueryableMockDbSet(libraries);
            var mockMarketDbSet = QueryableExtension.GetQueryableMockDbSet(market);
            var mockContext = new Mock<GXDBContext>();

            // Arrange behavior.
            mockContext.Setup(x => x.Libraries).Returns(mockLibrariesDbSet);
            mockContext.Setup(x => x.Market).Returns(mockMarketDbSet);
            mockContext.Setup(x => x.SaveChangesAsync(CancellationToken.None)).Returns(() => Task.Run(() => { return 1; })).Verifiable();

            // Arrange SUT.
            var playerService = new PlayerLibraryService(mockContext.Object);

            // Act.
            await playerService.SellGame(playerModel, gameModel);
        }

        [Fact]
        public async Task When_SellGameToMarketCostLowerThanZero_RaiseInvalidMoneyException()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var playerEntity = new PlayerEntity
            {
                Id = Guid.NewGuid(),
                Username = "Test",
                Money = 500,
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                Name = "Test",
                Publisher = publisherEntity,
                PublisherId = publisherEntity.Id,
                Source = "Test",
            };

            var library = new LibraryEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                GameId = gameEntity.Id,
                Player = playerEntity,
                PlayerId = playerEntity.Id,
            };

            var players = new List<PlayerEntity>
            {
                playerEntity,
            };

            var games = new List<GameEntity>
            {
                gameEntity,
            };

            var libraries = new List<LibraryEntity>
            {
                library,
            };

            var market = new List<MarketEntity>();

            var expected = new LibraryGameOutputModel
            {
                Id = library.Id.ToString(),
                Name = gameEntity.Name,
                Source = "Test",
            };

            var playerModel = new PlayerInputModel
            {
                Id = playerEntity.Id.ToString(),
            };

            var gameModel = new MarketSellGameInputModel
            {
                LibraryGameId = library.Id.ToString(),
                Cost = -10,
            };

            var mockLibrariesDbSet = QueryableExtension.GetQueryableMockDbSet(libraries);
            var mockMarketDbSet = QueryableExtension.GetQueryableMockDbSet(market);
            var mockGameDbSet = QueryableExtension.GetQueryableMockDbSet(games);
            var mockContext = new Mock<GXDBContext>();

            // Arrange behavior.
            mockContext.Setup(x => x.Libraries).Returns(mockLibrariesDbSet);
            mockContext.Setup(x => x.Market).Returns(mockMarketDbSet);
            mockContext.Setup(x => x.Games).Returns(mockGameDbSet);

            // Arrange SUT.
            var playerService = new PlayerLibraryService(mockContext.Object);

            // Assert.
            await Assert.ThrowsAsync<InvalidMoneyException>(() => playerService.SellGame(playerModel, gameModel));
        }
    }
}
