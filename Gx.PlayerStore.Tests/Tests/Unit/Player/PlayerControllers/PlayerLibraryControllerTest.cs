using GxPlayerStore.Controllers;
using GxPlayerStore.Models.Input;
using GxPlayerStore.Models.Output;
using GxPlayerStore.Services;
using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Gx.PlayerStore.Tests.Tests.Unit.Player.PlayerControllers
{
    public class PlayerLibraryControllerTest
    {
        [Fact]
        public async Task When_PlayerGetLibraryGames_ReturnListWithGames()
        {
            // Arrange.
            var id = Guid.NewGuid().ToString();

            var playerModel = new PlayerInputModel
            {
                Id = id,
            };

            var game = new LibraryGameOutputModel
            {
                Id = "1",
                Name = "Test",
                Source = "Test",
            };

            var games = new List<LibraryGameOutputModel>
            {
                game,
            };

            var expected = new List<LibraryGameOutputModel>
            {
                new LibraryGameOutputModel
                {
                    Id = "1",
                    Name = "Test",
                    Source = "Test",
                },
            };

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, playerModel.Id)
            };
            var identity = new ClaimsIdentity(claims, "Bearer");
            var claimsPrincipal = new ClaimsPrincipal(identity);

            var httpContext = new DefaultHttpContext
            {
                User = claimsPrincipal
            };


            var mockService = new Mock<PlayerLibraryService>(null);

            // Arrange behavior.
            mockService.Setup(x => x.GetLibraryGames(playerModel)).ReturnsAsync(games);

            // Arrange SUT.
            var controller = new PlayerLibraryController(mockService.Object)
            {
                ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext
                {
                    HttpContext = httpContext,
                },
            };

            // Act.
            var actual = await controller.GetLibraryGames(id);

            // Assert.
            //Assert.Equal(expected.Count, actual.Count);
        }
    }
}
